/* ES5 */
var isMomHappy = true;

// Promise
var willIGetNewPhone = new Promise(
    function (resolve, reject) {
        if (isMomHappy) {
            var phone = {
                brand: 'Samsung',
                color: 'black'
            };
            resolve(phone); // fulfilled
        } else {
            var reason = new Error('mom is not happy');
            reject(reason); // reject
        }

    }
);

// call our promise
var askMom = function () {
    willIGetNewPhone
        .then(function (fulfilled) {
            // yay, you got a new phone
            console.log(fulfilled);
            // output: { brand: 'Samsung', color: 'black' }
        })
        .catch(function (error) {
            // oops, mom don't buy it
            console.log(error.message);
            // output: 'mom is not happy'
        });
};


// jQuery Promise with getJson and callback
$(document).ready(function () {
    $('#get-data').click(function () {
        var showData = $('#show-data');

        $.getJSON('example.json', function (data) {
            console.log(data);

            var items = data.items.map(function (item) {
                return item.key + ': ' + item.value;
            });

            showData.empty();

            if (items.length) {
                var content = '<li>' + items.join('</li><li>') + '</li>';
                var list = $('<ul />').html(content);
                showData.append(list);
            }
        })
        .done(function() {
            console.log( "second success" );
        })
        .fail(function() {
            console.log( "error" );
        })
        .always(function() {
            console.log( "complete" );
        });

        showData.text('Loading the JSON file.');
    });

    //askMom();

});

/*
// setTimeOut and Promises
let promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("result");
    }, 1000);
});


promise
    .then(
        result => {
            alert("Fulfilled: " + result); // result
        },
        error => {
            alert("Rejected: " + error); // error
        }
    );

*/

/*

let promise = new Promise((resolve, reject) => {

    // через 1 секунду готов результат: result
    setTimeout(() => resolve("result"), 1000);

    // через 2 секунды — reject с ошибкой, он будет проигнорирован
    setTimeout(() => reject(new Error("ignored")), 2000);

});

promise
    .then(
        result => alert("Fulfilled: " + result), // сработает
        error => alert("Rejected: " + error) // не сработает
    );


*/

function httpGet(url) {

    return new Promise(function(resolve, reject) {

        console.log('Retrieving JSON data');

        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);

        xhr.onload = function() {
            if (this.status == 200) {
                resolve(this.response);
            } else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error("Network Error"));
        };

        xhr.send();
    });

}

/*
httpGet("example.json")
    .then(
        response => alert(`Fulfilled 1: ${response}`),
        error => alert(`Rejected: ${error}`)
    )
    .then(
        response => httpGet("example.json")
    )
    .then(
        response => alert(`Fulfilled 2: ${response}`),
        error => alert(`Rejected: ${error}`)
    )
    .then(
        response => httpGet("example.json")
    )
    .then(
        response => alert(`Fulfilled 3: ${response}`),
        error => alert(`Rejected: ${error}`)
    );
*/

function delay(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, ms);
    });
}

delay(500).then(function () {
    console.log('Hey!');
});

/*
let urls = [
    'example.json',
    'example_sec.json'
];

Promise.all( urls.map(httpGet) )
    .then(function(){
        alert('ok');
    });

    */

// Создаётся объект promise
let promise = new Promise((resolve, reject) => {

    setTimeout(() => {
        // переведёт промис в состояние fulfilled с результатом "result"
        resolve("result");
    }, 1000);

});


promise
    .then(
        result => {
            alert("Fulfilled: " + result);
        },
        error => {
            alert("Rejected: " + error);
        }
    );